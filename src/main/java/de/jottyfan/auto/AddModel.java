package de.jottyfan.auto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import de.jottyfan.primefaces.FilterBean;

/**
 * 
 * @author jotty
 *
 */
@Named
@SessionScoped
public class AddModel extends FilterBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(AddModel.class);

	@Inject
	private SessionBean sessionBean;

	private List<String> locations;
	private List<String> fuels;
	private List<String> providers;

	private AddBean addBean;

	private String key;

	public boolean startSession(FacesContext facesContext) {
		if (sessionBean == null) {
			sessionBean = new SessionBean();
			LOGGER.debug("created new sessionBean");
		}
		try {
			Gateway gw = new Gateway(facesContext);
			boolean newSession = sessionBean.startSession(gw.checkLogin(key));
			if (!newSession) {
				LOGGER.error("could not start session; session invalid");
				facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sitzung ungültig",
						"Die Sitzung konnte nicht gestartet werden."));
			} else {
				LOGGER.debug("start new session");
				locations = gw.getLocations();
				fuels = gw.getFuels();
				providers = gw.getProviders();
				resetAddBean();
			}
			return newSession;
		} catch (DataAccessException e) {
			LOGGER.error("error on starting session: {}", e.getMessage());
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Sitzung kaputt", e.getMessage()));
			return false;
		}
	}

	private void resetAddBean() {
		addBean = new AddBean();
		// setting defaults
		addBean.setFuel("E10");
		addBean.setLocation("Dresden");
		addBean.setProvider("Kaufland");
		addBean.setBuydate(LocalDateTime.now());
	}

	public void invalidateSession(FacesContext facesContext) {
		sessionBean.invalidate();
	}

	public boolean add(FacesContext facesContext) {
		try {
			Validator validator = new Validator();
			validator.validatePositive(addBean.getAmount());
			validator.validatePositive(addBean.getPrice());
			Gateway gw = new Gateway(facesContext);
			if (gw.add(addBean)) {
				facesContext.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_INFO, "neuer Datensatz", gw.getLastDataset()));
				resetAddBean();
				return true;
			} else {
				facesContext.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler", "Daten wurden nicht übertragen."));
				return false;
			}
		} catch (DataAccessException | ValidatorException e) {
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fehler", e.getMessage()));
			return false;
		}
	}

	public Boolean getInSession() {
		return sessionBean != null && sessionBean.isValid(); 
	}
	
	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	
	public List<String> filterFuels(String needles){
		return super.filter(fuels, needles, " "); // whitespace as default separator
	}
	
	public List<String> filterLocations(String needles){
		return super.filter(locations, needles, " "); // whitespace as default separator
	}

	public List<String> filterProviders(String needles){
		return super.filter(providers, needles, " "); // whitespace as default separator
	}

	public AddBean getAddBean() {
		return addBean;
	}
}
