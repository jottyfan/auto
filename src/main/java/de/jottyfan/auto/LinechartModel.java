package de.jottyfan.auto;

import java.io.Serializable;
import java.math.BigDecimal;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 * 
 * @author jotty
 *
 */
public class LinechartModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private LineChartModel zoomModel;

	public LinechartModel(LineChartSeries series, BigDecimal xMin, BigDecimal xMax) {
		zoomModel = new LineChartModel();
		zoomModel.addSeries(series);
		zoomModel.setTitle("Verbrauch");
		zoomModel.setZoom(true);
		zoomModel.setLegendPosition("e");
		Axis yAxis = zoomModel.getAxis(AxisType.Y);
		yAxis.setMin(xMin.subtract(new BigDecimal(0.10)));
		yAxis.setMax(xMax.add(new BigDecimal(0.10)));
	}

	/**
	 * @return the zoomModel
	 */
	public LineChartModel getZoomModel() {
		return zoomModel;
	}
}
