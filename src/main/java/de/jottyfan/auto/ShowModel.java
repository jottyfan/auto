package de.jottyfan.auto;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

/**
 * 
 * @author jotty
 *
 */
@Named
@SessionScoped
public class ShowModel implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger(ShowModel.class);

	private List<MileageViewBean> data;

	/**
	 * load data from database
	 * 
	 * @param facesContext
	 */
	public void loadData(FacesContext facesContext) {
		try {
			data = new Gateway(facesContext).getData();
			LOGGER.debug("data loaded");
		} catch (DataAccessException e) {
			LOGGER.error("error on loading data: {}", e.getMessage());
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Datenbankfehler", e.getMessage()));
		}
	}

	public List<MileageViewBean> getData() {
		return data;
	}
}
