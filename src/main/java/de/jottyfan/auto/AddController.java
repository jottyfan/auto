package de.jottyfan.auto;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class AddController {

	@Inject
	private AddModel addModel;

	@Inject
	private ShowModel showModel;

	public String doStartSession() {
		boolean newSession = addModel.startSession(FacesContext.getCurrentInstance());
		return newSession ? toWelcome() : "/pages/login.jsf";
	}

	public String doInvalidateSession() {
		addModel.invalidateSession(FacesContext.getCurrentInstance());
		addModel.setKey(null);
		return "/pages/login.jsf";
	}

	public String toWelcome() {
		showModel.loadData(FacesContext.getCurrentInstance());
		return "/pages/welcome.jsf";
	}

	public String toAdd() {
		showModel.loadData(FacesContext.getCurrentInstance());
		return "/pages/add.jsf";
	}

	public String doAdd() {
		addModel.add(FacesContext.getCurrentInstance());
		return toWelcome();
	}

	public AddModel getAddModel() {
		return addModel;
	}

	public void setAddModel(AddModel addModel) {
		this.addModel = addModel;
	}

	public ShowModel getShowModel() {
		return showModel;
	}

	public void setShowModel(ShowModel showModel) {
		this.showModel = showModel;
	}
}
