package de.jottyfan.auto;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class EditController {

	@Inject
	private EditModel editModel;

	@Inject
	private ShowModel showModel;

	public String toEdit(MileageViewBean bean) {
		if (editModel == null) {
			editModel = new EditModel();
		}
		editModel.setBean(bean);
		return "/pages/edit.jsf";
	}

	public String doUpdate() {
		boolean result = editModel.update(FacesContext.getCurrentInstance());
		if (result) {
			showModel.loadData(FacesContext.getCurrentInstance());
			return "/pages/add.jsf";
		} else {
			return "";
		}
	}

	public EditModel getModel() {
		return editModel;
	}

	public void setModel(EditModel editModel) {
		this.editModel = editModel;
	}

	public ShowModel getShowModel() {
		return showModel;
	}

	public void setShowModel(ShowModel showModel) {
		this.showModel = showModel;
	}
}
