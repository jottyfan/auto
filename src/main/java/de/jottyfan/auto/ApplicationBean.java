package de.jottyfan.auto;

import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 * 
 * @author jotty
 *
 */
@Named
@RequestScoped
public class ApplicationBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * get version from maven build
	 * 
	 * @param facesContext
	 * @return
	 */
	public String getVersion(FacesContext facesContext) {
		final Properties prop = new Properties();
		try {
			prop.load(facesContext.getExternalContext().getResourceAsStream("/META-INF/MANIFEST.MF"));
			String version = prop.getProperty("Implementation-Version");
			return new StringBuilder("Version ").append(version == null ? prop.toString() : version).toString();
		} catch (final IOException e) {
			return e.getMessage();
		}
	}

	/**
	 * get all log levels
	 * 
	 * @return the log levels
	 */
	public Level[] getLoglevels() {
		return Level.values();
	}

	/**
	 * get the current log level
	 * 
	 * @return the current log level
	 */
	public String getLoglevel() {
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration config = ctx.getConfiguration();
		LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
		Level level = loggerConfig.getLevel();
		return level.name();
	}

	/**
	 * set the loglevel
	 * 
	 * @param loglevel
	 *            the loglevel
	 */
	public void setLoglevel(Level loglevel) {
		LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		Configuration config = ctx.getConfiguration();
		LoggerConfig loggerConfig = config.getLoggerConfig(LogManager.ROOT_LOGGER_NAME);
		loggerConfig.setLevel(loglevel);
		ctx.updateLoggers();
	}
}
