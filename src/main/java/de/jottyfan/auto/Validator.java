package de.jottyfan.auto;

import java.math.BigDecimal;

/**
 * 
 * @author jotty
 *
 */
public class Validator {

	/**
	 * throw an exception if value <= 0; null may be a valid one
	 * 
	 * @param value
	 *            the value
	 * @throws ValidatorException
	 *             if value <= 0
	 */
	public void validatePositive(BigDecimal value) throws ValidatorException {
		if (value != null && value.doubleValue() <= 0.0f) {
			throw new ValidatorException(value + " <= 0");
		}
	}
}
