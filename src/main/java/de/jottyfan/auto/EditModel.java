package de.jottyfan.auto;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.jooq.exception.DataAccessException;

/**
 * 
 * @author jotty
 *
 */
@Named
@SessionScoped
public class EditModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private MileageViewBean bean;

	public MileageViewBean getBean() {
		return bean;
	}

	public void setBean(MileageViewBean bean) {
		this.bean = bean;
	}

	/**
	 * update bean in db
	 * 
	 * @param facesContext
	 * @return true for successful update, false otherwise
	 */
	public boolean update(FacesContext facesContext) {
		try {
			if (!new Gateway(facesContext).update(bean)) {
				throw new DataAccessException("keine Daten zum Aktualisieren gefunden");
			}
			return true;
		} catch (DataAccessException e) {
			facesContext.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "update error", e.getMessage()));
			return false;
		}
	}
}
