package de.jottyfan.auto;

import java.math.BigDecimal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.LineChartSeries;

@Named
@RequestScoped
public class LinechartController {
	
	@Inject
	private ShowModel showModel;
	
	private LinechartModel model;
	
	public String toLinechart() {
		LineChartSeries series = new LineChartSeries("Auto 1");
		BigDecimal xMax = new BigDecimal(0);
		BigDecimal xMin = new BigDecimal(10);
		for (MileageViewBean bean : showModel.getData()) {
			series.set(bean.getMileage(), bean.getEuroproliter());
			xMax = xMax.floatValue() < bean.getEuroproliter().floatValue() ? bean.getEuroproliter() : xMax;
			xMin = xMin.floatValue() > bean.getEuroproliter().floatValue() ? bean.getEuroproliter() : xMin;
		}
		model = new LinechartModel(series, xMin, xMax);
		return "/pages/linechart.jsf";
	}

	public LinechartModel getModel() {
		return model;
	}
}
