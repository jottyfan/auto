package de.jottyfan.auto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 
 * @author jotty
 *
 */
public class AddBean {

	private Integer mileage;
	private String fuel;
	private String location;
	private BigDecimal price;
	private BigDecimal amount;
	private String provider;
	private LocalDateTime buydate;
	private String annotation;

	public Integer getMileage() {
		return mileage;
	}

	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}

	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public LocalDateTime getBuydate() {
		return buydate;
	}

	public void setBuydate(LocalDateTime buydate) {
		this.buydate = buydate;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}
}
