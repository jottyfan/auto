package de.jottyfan.primefaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author jotty
 *
 */
public abstract class FilterBean implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * filter the query by looking up each word in the list; if one word is found,
	 * the line is added
	 * 
	 * @param list
	 *            the list of haystacks
	 * @param query
	 *            the needles separated by the splitseparator
	 * @param splitseparator
	 *            a character to split the needles by
	 * @return the filtered list
	 */
	public List<String> filter(List<String> list, String query, String splitseparator) {
		List<String> result = new ArrayList<>();
		for (String s : list) {
			if (s == null || s.isBlank()) {
				// do nothing
			} else {
				String[] needles = s.split(splitseparator);
				for (String needle : needles) {
					if (needle.isBlank()) {
						// do nothing
					} else if (needle.toLowerCase().contains(query.toLowerCase())) {
						result.add(s);
					}
				}
			}
		}
		return result;
	}
}
